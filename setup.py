# -*- coding: utf-8 -*-
#
# webscraper setup script
#
# Copyright (C) 2019-2020 Sathya Kuppuswamy
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# @Author  : Sathya Kupppuswamy(sathyaosid@gmail.com)
# @History :
#            @v0.0 - Initial update
# @TODO    :
#
#

from setuptools import setup

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(name='webscraper',
      version='0.1',
      description='Python support classes for parsing web',
      long_description=readme,
      classifiers=[
          'Development Status :: 1 - Planning',
          'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
          'Programming Language :: Python :: 3.5',
          'Topic :: Text Processing :: Linguistic',
      ],
      keywords='python git shell linux',
      url='https://gitlab.com/knsathya/webscraper.git',
      author='Kuppuswamy Sathyanarayanan',
      author_email='sathyaosid@gmail.com',
      license='GPLv2',
      packages=['webscraper', 'app'],
      install_requires=[
          'bs4',
          'urllib3',
          'urlparse3',
          'logging3',
          'cfscrape',
          'requests',
          'selenium',
          'genshi',
          'lxml',
          'webdriver-manager',
          'click'
      ],
      dependency_links=[
      ],
      test_suite='tests',
      tests_require=[
          ''
      ],
      entry_points={
          'console_scripts': ['webscrape = app.webscrape:cli'],
      },
      include_package_data=True,
      zip_safe=False)
