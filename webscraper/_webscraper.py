from bs4 import BeautifulSoup
import time
import logging
import urllib
import random
import requests
import cfscrape

try:
    # python 3
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

user_agent_list = [
   #Chrome
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
    #Firefox
    'Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
    'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)'
]

class webparser(object):
    def __init__(self, html, name="webparser", encode='', logger=None):
        self.logger = logger or logging.getLogger("webparser")
        self.name = name
        if encode in ['utf-8', 'ascii']:
            self.encode = encode
        else:
            self.encode = None
        self._do_parse(html)

    def _encode(self, data):
        if self.encode:
            if isinstance(data, (list, tuple)):
                for index,entry in enumerate(data):
                    data[index] = entry.encode(self.encode)
            else:
                data = data.encode(self.encode)

        return data

    def _do_parse(self, html):
        self.html = html
        self.soup = BeautifulSoup(self.html, 'lxml')

    def reparse(self, html):
        self._do_parse(html)

    def get_title(self):
        return self._encode(self.soup.title.text)

    def get_ptext(self):
        visible_text = self.soup.find_all('p')
        valid_text = []
        for tag in visible_text:
            valid_text.append(tag.text)

        return self._encode(valid_text)

    def get_alinks(self):
        link_text = self.soup.find_all('a', href=True)
        valid_text = []
        for tag in link_text:
            valid_text.append(tag['href'])

        return self._encode(valid_text)

    def get_all_tag_attr(self, tag, attr={}, gattr=''):
        if attr:
            visible_text = self.soup.find_all(tag, attr)
        else:
            visible_text = self.soup.find_all(tag)

        valid_text = []
        for tag in visible_text:
            valid_text.append(tag[gattr] if len(gattr) > 0 else tag.text)

        return self._encode(valid_text)

    def get_all_tag_attr_grouped(self, tag, attr={}, gattr=''):
        if attr:
            visible_text = self.soup.find_all(tag, attr)
        else:
            visible_text = self.soup.find_all(tag)

        valid_text = []
        for tag in visible_text:
            block_text = []
            block_text.append(tag[gattr] if len(gattr) > 0 else tag.text)
            valid_text.append(self._encode(block_text))

        return valid_text

    def get_ptext_from_tag(self, tag, attr={}):
        if attr:
            visible_text = self.soup.findAll(tag, attr)
        else:
            visible_text = self.soup.findAll(tag)

        valid_text = []
        for ntag in visible_text:
            ptags = ntag.find_all('p')
            for ptag in ptags:
                valid_text.append(ptag.text)

        return self._encode(valid_text)

    def __str__(self):
        out_str = ""
        out_str += "Parser Name           : " + self.name + "\n"

        return out_str

class webconnect(object):

    CONN_LIBS=["urllib", "requests", "cfscrape", "firefox", "phantomjs", "chrome"]

    def _get_urllib(self):
        try:
            if self.session is None:
                self.session = urllib.request
            request = urllib.request.Request(self.url, headers={'User-Agent': random.choice(user_agent_list)})
            response = urllib.request.urlopen(request, timeout=self.timeout)
            self.html = response.read()
            return True
        except Exception as e:
            self.logger.error('urllib failed retry %s', self.url)
            self.logger.error(e)
            return False

    def _get_requests(self):
        try:
            if self.session is None:
                self.session = requests
            response = requests.get(self.url, timeout=self.timeout, headers={'User-Agent': random.choice(user_agent_list)})
            self.html = response.content
            return True
        except Exception as e:
            self.logger.error('requests failed retry %s', self.url)
            self.logger.error(e)
            return False

    def _get_cfscrape(self):
        try:
            scraper = cfscrape.create_scraper(delay=self.timeout)
            if self.session is None:
                self.session = scraper
            request = scraper.get(self.url)
            self.html = request.content
            return True
        except Exception as e:
            self.logger.error('requests failed retry %s', self.url)
            self.logger.error(e)
            return False

    def _get_firefox(self):
        try:
            options = webdriver.firefox.options.Options()
            if self.headless:
                options.add_argument("--headless")
            if self.session is None:
                self.session = webdriver.Firefox(firefox_options=options)
            self.session.get(self.url)
            self.html = self.session.page_source
            return True
        except Exception as e:
            self.logger.error('firefox failed retry %s', self.url)
            self.logger.error(e)
            return False

    def _get_chrome(self):
        try:
            options = webdriver.chrome.options.Options()
            if self.headless:
                options.add_argument("--headless")
            if self.session is None:
                self.session = webdriver.Chrome(chrome_options=options)
            self.session.get(self.url)
            self.html = self.session.page_source
            return True
        except Exception as e:
            self.logger.error('chrome failed retry %s', self.url)
            self.logger.error(e)
            return False

    def _get_phanthomjs(self):
        try:
            if self.session is None:
                self.session = webdriver.PhantomJS()
            self.session.get(self.url)
            self.html = self.session.page_source
            return True
        except Exception as e:
            self.logger.error('PhantomJS failed retry %s', self.url)
            self.logger.error(e)
            return False

    def get_url(self, url, conn_lib=None, timeout=None, headless=None, retry=1):
        if conn_lib is not None:
            self.conn_lib = conn_lib
        if timeout is not None:
            self.timeout = timeout
        if headless is not None:
            self.headless = headless

        self.url = url

        self.logger.info("Using %s reading %s\n", conn_lib, url)
        retry = retry + 1


        while retry:
            status = False
            if self.conn_lib == "urllib":
                status = self._get_urllib()
            elif self.conn_lib == "requests":
                status = self._get_requests()
            elif self.conn_lib == "cfscrape":
                status = self._get_cfscrape()
            elif self.conn_lib == "firefox":
                status = self._get_firefox()
            elif self.conn_lib == "chrome":
                status = self._get_chrome()
            elif self.conn_lib == "phantomjs":
                status = self._get_phanthomjs()

            if not status:
                retry = retry - 1
            else:
                retry = 0

    def _url_validator(self, url):
        if len(url) <= 0:
            return False
        try:
            result = urlparse(url)
            print(result)
            return all([result.scheme, result.netloc])
        except:
            return False

    def __init__(self, url, name="webconnect", conn_lib="urllib", timeout=20, headless=True, logger=None):
        self.logger = logger or logging.getLogger("webconnect")

        if conn_lib not in webconnect.CONN_LIBS:
            raise ValueError("%s Not supported, available connection types %s" %
                             (conn_lib, ','.join(webconnect.CONN_LIBS)))

        if not self._url_validator(url):
            raise ValueError("Invalid URL %s" % url)

        self.name = name
        self.url = url
        self.conn_lib = conn_lib
        self.domain = '{uri.scheme}://{uri.netloc}/'.format(uri=urlparse(self.url))
        self.session = None
        self.title = "Demo Title"
        self.html = None
        self.headless = headless
        self.timeout = timeout

        self.get_url(url, conn_lib, timeout, headless, retry=3)

    def del_session(self):
        self.session = None

    def do_click(self, xpath_str=None, class_str=None):
        if self.conn_lib not in ["firefox", "chrome"]:
            return
        if xpath_str is not None:
            self.session.find_element_by_xpath(xpath_str).click()
        if class_str is not None:
            self.session.find_element_by_class_name(class_str).click()

        self.html = self.session.page_source

    def wait_for_element(self, ename, etype="ID", timeout=30):

        if etype not in ["ID", "CLASS"]:
            self.logger.error("Element type not supported\n")
            return

        if etype == "ID":
            WebDriverWait(self.session, timeout).until(
                EC.presence_of_element_located((By.ID, ename)))
        elif etype == "CLASS":
            self.logger.info("Waiting for class %s", ename)
            WebDriverWait(self.session, timeout).until(
                EC.presence_of_element_located((By.CLASS_NAME, ename)))

        self.html = self.session.page_source

    def page_scroll_down(self):

        if self.conn_lib not in ["firefox", "chrome"]:
            self.logger.error("Browser %s is not supported\n", self.conn_lib)
            return

        SCROLL_PAUSE_TIME = 2
        while True:
            # Get scroll height
            ### This is the difference. Moving this *inside* the loop
            ### means that it checks if scrollTo is still scrolling
            last_height = self.session.execute_script("return document.body.scrollHeight")

            # Scroll down to bottom
            self.session.execute_script("window.scrollTo(0, document.body.scrollHeight);")

            # Wait to load page
            time.sleep(SCROLL_PAUSE_TIME)

            # Calculate new scroll height and compare with last scroll height
            new_height = self.session.execute_script("return document.body.scrollHeight")
            if new_height == last_height:

                # try again (can be removed)
                self.session.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                # Wait to load page
                time.sleep(SCROLL_PAUSE_TIME)

                # Calculate new scroll height and compare with last scroll height
                new_height = self.session.execute_script("return document.body.scrollHeight")

                # check if the page height has remained the same
                if new_height == last_height:
                    # if so, you are done
                    break
                # if not, move on to the next loop
                else:
                    last_height = new_height
                    continue

        self.html = self.session.page_source

    def __str__(self):
        out_str = ""
        out_str += "Browser Name           : " + self.name + "\n"
        out_str += "Connection Lib        : " + self.conn_lib + "\n"

        return out_str

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger("TLParser")
    logger.setLevel(logging.INFO)

    session = webconnect(url="https://www.zhaishuyuan.com/chapter/25664/15212673", conn_lib="firefox", headless=False)
    #parser = webparser(session.html, encode='utf-8')
    parser = webparser(session.html)
    data = parser.get_ptext_from_tag(tag='div', attr={'id':'content'})
    #print([item.decode('utf-8') for item in data])
    print(data)
