#!/usr/bin/env python3
import click
import webscraper


@click.command()
@click.argument('url', nargs=1)
@click.option('--conn-lib', default='firefox', type=click.Choice(webscraper.webconnect.CONN_LIBS), help='Connection library')
@click.option('--print-tag', '-p', is_flag=True, help="Print tag output.")
@click.option('--tag', multiple=True, type=click.Tuple([str, str, str]), help='Tag to print in "Tag-Name Attr-Name Attr-Val" format')

def cli(url, conn_lib, print_tag, tag):
    session = webscraper.webconnect(url=url, conn_lib=conn_lib, headless=False)
    if not session:
        print("Invalid argument")
        return
    parser = webscraper.webparser(session.html)
    data = 'None'
    if tag:
        if len(tag) > 1:
            data = parser.get_ptext_from_tag(tag=tag[0], attr={tag[1]:tag[2]})
        else:
            data = parser.get_ptext_from_tag(tag=tag[0])
    else:
        data = parser.html

    if print_tag:
        print(data)

if __name__ == '__main__':
    cli()